package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class customerController {

    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void employeeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent employeeScreen = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(employeeScreen));
        primaryStage.show();
    }

    @FXML
    public void orderButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent orderScreen = FXMLLoader.load(getClass().getResource("orders.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(orderScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void viewButtonClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("viewCustomer.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Customers");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void addButtonClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("addCustomer2.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Customers");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void editButtonClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("addCustomer2.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Customers");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void deleteButtonClicked(ActionEvent actionEvent) throws IOException{

    }



}