package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class orderController {
    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void customerButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent customerScreen = FXMLLoader.load(getClass().getResource("customer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(customerScreen));
        primaryStage.show();
    }

    @FXML
    public void employeeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent employeeScreen = FXMLLoader.load(getClass().getResource("employee.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(employeeScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void viewBtnOrderClicked (ActionEvent actionEvent) throws IOException {

    }

    @FXML
    public void addBtnOrderClicked (ActionEvent actionEvent) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("addOrder2.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Orders");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void editBtnOrderClicked (ActionEvent actionEvent) throws IOException {

    }

    @FXML
    public void deleteBtnOrderClicked (ActionEvent actionEvent) throws IOException {

    }



}
