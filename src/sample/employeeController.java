package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class employeeController {

    @FXML
    public void homeButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent mainScreen = FXMLLoader.load(getClass().getResource("mainScreen.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(mainScreen));
        primaryStage.show();
    }

    @FXML
    public void customerButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent customerScreen = FXMLLoader.load(getClass().getResource("customer.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(customerScreen));
        primaryStage.show();
    }

    @FXML
    public void orderButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent orderScreen = FXMLLoader.load(getClass().getResource("orders.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(orderScreen));
        primaryStage.show();
    }

    @FXML
    public void productButtonClicked(ActionEvent actionEvent) throws IOException {
        Parent productScreen = FXMLLoader.load(getClass().getResource("products.fxml"));
        Stage primaryStage = (Stage)((Node)actionEvent.getSource()).getScene().getWindow();
        primaryStage.setScene(new Scene(productScreen));
        primaryStage.show();
    }

    @FXML
    public void viewBtnEmployeeClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("viewEmployee.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Employees");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void addBtnEmployeeClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("addEmployee2.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Employees");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void editBtnEmployeeClicked(ActionEvent actionEvent) throws IOException{
        FXMLLoader fxmlLoader = new FXMLLoader();
        Parent root = fxmlLoader.load(getClass().getResource("addEmployee2.fxml"));
        Stage stage = new Stage();
        stage.setTitle("Employees");
        stage.setScene(new Scene(root));
        stage.showAndWait();

    }

    @FXML
    public void deleteBtnEmployeeClicked(ActionEvent actionEvent) throws IOException{

    }



}